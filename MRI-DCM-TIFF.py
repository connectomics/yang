import os
import pydicom
import imageio
from PIL import Image

def convert_dcm_to_tiff(input_folder, output_folder):
    # Traverse the input folder
    for root, dirs, files in os.walk(input_folder):
        for file in files:
            # Check if the file is a .dcm file
            if file.endswith(".dcm"):
                dcm_path = os.path.join(root, file)
                
                # Read the DICOM file
                dicom_data = pydicom.dcmread(dcm_path)
                
                # Extract the file name (without extension)
                file_name = os.path.splitext(file)[0]
                
                # Build the output folder path, maintaining the same structure
                relative_path = os.path.relpath(dcm_path, input_folder)
                output_subfolder = os.path.join(output_folder, os.path.dirname(relative_path))
                
                # Create the output folder (if it does not exist)
                os.makedirs(output_subfolder, exist_ok=True)
                
                # Build the path for the output tiff file
                tiff_path = os.path.join(output_subfolder, f"{file_name}.tiff")
                
                # Save the DICOM file as a tiff format
                imageio.imwrite(tiff_path, dicom_data.pixel_array)

def merge_tiff_files(input_folder, output_path):
    # Traverse the input folder
    tiff_images = []
    for root, dirs, files in os.walk(input_folder):
        for file in files:
            # Check if the file is a .tiff file
            if file.endswith(".tiff"):
                tiff_path = os.path.join(root, file)
                # Open the tiff file and append it to the list
                tiff_images.append(Image.open(tiff_path))

    # Merge all tiff images
    if tiff_images:
        # Get the size and mode of the first image
        base_image = tiff_images[0]
        # Create a new Image object containing all tiff images
        base_image.save(output_path, save_all=True, append_images=tiff_images[1:])

if __name__ == "__main__":
    # Input folder containing subfolders
    input_folder = "/path/to/your/input/folder"
    
    # Output folder
    output_folder = "/path/to/your/output/folder"
    
    # Path for the merged tiff file
    merged_tiff_path = "/path/to/your/merged/output.tiff"
    
    # Call the function to convert
    convert_dcm_to_tiff(input_folder, output_folder)
    
    # Merge all tiff files
    merge_tiff_files(output_folder, merged_tiff_path)
